import torch
import torch.nn as nn
from torch import Tensor

device = "cpu"

class FeedForward(torch.nn.Module):
    def __init__(self, input_dim, output_dim, dropout, bias=True):
        super().__init__()
        self.net = nn.Sequential(
            nn.Linear(input_dim, 4 * input_dim, bias=bias),
            nn.ReLU(),
            nn.Linear(4 * input_dim, output_dim, bias=bias),
            nn.Dropout(dropout),
        )

    def forward(self, x):
        return self.net(x)

class Block(nn.Module):
    def __init__(self, d_in, d_out, dropout=0.2):
        super().__init__()
        self.d_in = d_in
        self.d_out = d_out
        self.ffwd = FeedForward(d_in, d_out, dropout=dropout)
        self.ln = nn.LayerNorm(d_in)

    def forward(self, x):
        return x + self.ffwd(self.ln(x))

class FFNorm(nn.Module):
    def __init__(self, d_in, d_out, dropout=0.2, with_sigmoid=False):
        super().__init__()
        self.d_in = d_in
        self.d_out = d_out
        dropout = dropout
        self.last_layer = nn.Linear(d_in, d_out, bias=True)
        self.net = nn.Sequential(
            Block(d_in, d_in, dropout=dropout),
            Block(d_in, d_in, dropout=dropout),
            Block(d_in, d_in, dropout=dropout),
            self.last_layer
        )
        if with_sigmoid:
            self.net.append(nn.Sigmoid())

    def forward(self, x):
        if isinstance(x, Tensor):
            return self.net(x.to(device))
        return self.net(Tensor(x).to(device))
